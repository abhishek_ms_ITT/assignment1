var canvas = document.querySelector('canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext('2d');

c.moveTo(0, 0);
c.lineTo(200, 100);
c.stroke();


c.font = "50px Arial";
c.fillText("Hello World", 300, 50);

c.font = "50px Arial";
c.strokeText("Hello World", 600, 50);

c.moveTo(250, 100);
c.lineTo(300, 200);
c.lineTo(100,400);

c.stroke();
/*
var x = 200;
var dx = 11;
var rad = 40;

function animate()
{
    requestAnimationFrame(animate)
    c.clearRect(0,0,innerWidth,innerHeight); //to clear the screen for every loop
    c.beginPath(); 
    c.arc(x,200,rad,Math.PI*2,false); //to draw circle
    c.strokeStyle = 'black';
    c.stroke();
    x+=dx;

    if(x > innerWidth || x < 0)
    {
        dx = -dx;
    }
}
animate();
*/

